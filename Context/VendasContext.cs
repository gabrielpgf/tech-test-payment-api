using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class VendasContext : DbContext
    {
        public VendasContext(DbContextOptions<VendasContext> options) : base(options)
        {
        }

        public DbSet<Produto> Produtos {get; set;}
        public DbSet<Venda> Vendas {get; set;}
        public DbSet<Vendedor> Vendedores {get; set;}               
    }
}