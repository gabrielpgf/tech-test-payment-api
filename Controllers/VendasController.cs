using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly VendasContext _context;

        public VendasController(VendasContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            if (venda.Status != EnumStatusVenda.AguardandoPagamento)
                return BadRequest(new {Erro = "Erro: Só é possível cadastrar uma venda se o status for 'Aguardando Pagamento'"});
            
            
            _context.Add(venda);            
            _context.SaveChanges();
            return Ok(venda);
        }

        [HttpGet("ListarVendasPorId")]
        public IActionResult BuscarVenda(int id)
        {
            var venda = _context.Vendas
                .Where(p => p.Id == id)
                .Select(p => new {
                    Id = p.Id,
                    DataPedido = p.DataPedido,
                    NomeVendedor = p.Vendedor.Nome,
                    NomeProduto = p.Produtos,
                    Status = p.Status.ToString()
                });

            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

        [HttpPut("AtualizaStatusVenda")]
        public IActionResult AtualizarVenda(int id, EnumStatusVenda status)
        {
            var vendasBanco = _context.Vendas.Find(id);

            if (vendasBanco == null)
                return NotFound();

            EnumStatusVenda statusAtual = vendasBanco.Status;

            switch(statusAtual)
            {
                case EnumStatusVenda.AguardandoPagamento:
                    if (status != EnumStatusVenda.PagamentoAprovado && status != EnumStatusVenda.Cancelada)
                        return BadRequest(new { Erro = "Atualização disponível apenas para o Status 'Pagamento Aprovado' e 'Cancelada'."});
                    vendasBanco.Status = status;                    
                    break; 
                case EnumStatusVenda.PagamentoAprovado:
                    if (status != EnumStatusVenda.EnviadoParaTransportadora && status != EnumStatusVenda.Cancelada)
                        return BadRequest(new { Erro = "Atualização disponível apenas para o Status 'Enviado para a Transportadora' e 'Cancelada'."});
                    vendasBanco.Status = status;
                    break;
                case EnumStatusVenda.EnviadoParaTransportadora:
                    if (status != EnumStatusVenda.Entregue)
                        return BadRequest(new { Erro = "Atualização disponível apenas para o Status 'Entregue'."});
                    vendasBanco.Status = status;
                    break;
                default:
                    return BadRequest(new { Erro = "Só é possível atualizar os Status: Aguardando Pagamento, Pagamento Aprovado e Entregue."});
                    break;
            }

           
            _context.Vendas.Update(vendasBanco);
            _context.SaveChanges();
            return Ok(BuscarVenda(id));            
        }        
    }
}